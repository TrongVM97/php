Tên dự án: Quản lý máy;
CHẠY demo.php để TEST ALL FEATURES
Chức năng chính: 
+ Thêm máy vào kho
    + Nhập mã máy;
    + Thêm chi tiết đơn vào máy;
    + Thêm chi tiết phúc vào máy;
+ Xuất thông tin kho;
+ Tìm kiếm và xuất ra tổng tiền máy theo mã máy;
+  Tìm kiếm và xuất ra tổng khối lượng máy theo mã máy;

Chi tiết:
1. Nhập chi tiết đơn:
 + Nhập giá: giá phải là số, dương; đơn vị tính là VN
    + Case 1: Số nguyên, số thực dương: 1200, 1200.5
    + Case 2: Số nguyên, số thực âm: -1200.5, -1200
    + Case 3: Ký tự: lab, lab123, 123lab, muoi mot ngh
 + Nhập khối lượng: khối lương là số, dương, đơn vị tính là KG;
    + Case 1: Số nguyên số thực dương : 3.335, 3;
    + Case 2: Số nguyên , số thực âm: -3.5, -300
    + Case 3: Ký tự 1200 KG, muoi hai kilogam;
2. Nhập chi tiết phức:
    + Case 1: Đúng option 1,2,0
    + Case 2: Không đúng option: 123abc,abc, abc123
3. Command line lựa chon theo option nói chung:
    + Case 1: Đúng options 1,2,3,4
    + Case 2:  Không đúng option: 123abc,abc, abc123

