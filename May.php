<?php
require_once 'ChiTiet.php';
require_once 'ChiTietDon.php';
require_once 'ChiTietPhuc.php';

class May
{
    /**
     * @var varchar
     */
    public $maSoMay;

    /**
     * @var array
     */
    public $danhSachChiTiet =[];

    /**
     * @param array $danhSachChiTiet
     */
    public function setDanhSachChiTiet($danhSachChiTiet)
    {
        $this -> danhSachChiTiet = $danhSachChiTiet;
    }

    /**
     * @return void
     */
    public function nhap(){
        $this->maSoMay = readline ('Nhap ma so may (VD: MC_01, MC_02,...)');
        while (true){
            echo "\e[34m";
            $loaiChiTiet = readline ("1.Chi Tiet Don, 2.Chi Tiet Phuc, 0 thoat. Ban muon nhap chi tiet nao ??(chi 1 hoac 2 hoac 0)");
            echo "\033[0m";
            switch ($loaiChiTiet){
                case "1":
                    $item = new ChiTietDon();
                    $item->nhap ();
                    $this->danhSachChiTiet[]=$item;
                    break;
                case "2":
                    $item = new ChiTietPhuc();
                    $item->nhap ();
                    $this->danhSachChiTiet[]=$item;
                    break;
                case "0":
                    echo "\e[31m STOP: dung lai qua trinh nhap chi tiet MAY ....\n";
                    echo "\033[0m";
                    return;
                default:
                    echo  "\e[31m Nhap sai dinh dang, vui long nhap lai \n";
                    echo "\033[0m";
                    break;

            }

        }
    }

    /**
     * @param varchar $maSoMay
     */
    public function setMaSoMay($maSoMay)
    {
        $this -> maSoMay = $maSoMay;
    }

    /**
     * @return void
     */
    public function xuat(){
        echo "\e[33m ***May ".$this->maSoMay." ,bao gom: \e[0m \n";
        for ($i=0;$i< count ($this->danhSachChiTiet);$i++){
            $this->danhSachChiTiet[$i]->xuat();
        }
    }

    /**
     * @param $maSoMay
     * @return bool
     */
    public function getMaSoMay($maSoMay) :bool {
        return $this->maSoMay == $maSoMay;
    }

    /**
     * @return int
     */
    public function tinhTongTien():int {
        $tong=0;
        for ($i=0;$i< count ($this->danhSachChiTiet);$i++){
            $tong+= $this->danhSachChiTiet[$i]->tinhTongTien();
        }
        return $tong;
    }

    /**
     * @return int
     */
    public function tinhTongKhoiLuong ():int {
        $tong=0;
        for ($i=0;$i< count ($this->danhSachChiTiet);$i++){
            $tong+= $this->danhSachChiTiet[$i]->tinhTongKhoiLuong();
        }
        return $tong;
    }
}


