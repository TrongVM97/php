<?php

abstract class ChiTiet
{
    /**
     * @var varchar
     */
    protected $maSoChiTiet;

    public function nhap(){
        $this->maSoChiTiet = readline ('Nhap ma so chi tiet (VD:CTD01,CTP01,..)');
    }

    /**
     * @return void
     */
    public  function xuat(){
        echo '====> Chi Tiet '.$this->maSoChiTiet."\n";
    }

    /**
     * @param varchar $maSoChiTiet
     */
    public function setMaSoChiTiet($maSoChiTiet)
    {
        $this -> maSoChiTiet = $maSoChiTiet;
    }

    /**
     * @return int
     */
    public abstract function tinhTongTien():int ;

    /**
     * @return float
     */
    public abstract function tinhTongKhoiLuong():float;

}