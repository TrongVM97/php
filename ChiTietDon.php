<?php
require_once 'ChiTiet.php';

class ChiTietDon extends ChiTiet
{
    /**
     * @var integer
     */
    public $giaBan;
    /**
     * @var float
     */

    public $khoiLuong;

    /**
     * @return void
     */
    public function nhap()
    {
        parent ::nhap ();
        do{
            $this->giaBan = readline('Nhap gia ban VND VD 1200, 1300, ...');
        }
        while (!is_int ($this->giaBan) && !(((int)$this->giaBan)>0));

        do{
            $this->khoiLuong =readline ('Nhap khoi luong KG(VD:12,12.4,...');
        }
        while (!is_float ($this->khoiLuong) && !(((float)$this->khoiLuong)>0));
    }

    /**
     * @return void
     */
    public function xuat()
    {
        parent::xuat ();
        echo '=========>Gia ban cua '.$this->maSoChiTiet.' la '.$this->giaBan."\n";
        echo '=========>Khoi luong cua '.$this->maSoChiTiet.' la '.$this->khoiLuong."\n";
    }

    /**
     * @param varchar  $maSoChiTiet
     * @return void
     */
    public function setMaSoChiTiet($maSoChiTiet)
    {
        $this -> maSoChiTiet = $maSoChiTiet;
    }

    /**
     * @param  $giaBan
     */
    public function setGiaBan($giaBan)
    {
        $this -> giaBan = $giaBan;
    }

    /**
     * @param float $khoiLuong
     * @return void
     */
    public function setKhoiLuong($khoiLuong)
    {
        $this->khoiLuong = $khoiLuong;
    }

    /**
     * @return int
     */
    public function tinhTongTien(): int
    {
       return $this->giaBan;
    }

    /**
     * @return float
     */
    public function tinhTongKhoiLuong(): float
    {
       return  $this->khoiLuong;
    }
}
