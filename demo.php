<?php
require_once 'ChiTiet.php';
require_once 'ChiTietDon.php';
require_once 'ChiTietPhuc.php';
require_once  'May.php';
require_once 'Kho.php';

    /**
     * class: ChiTietDon
     * ma so chi tiet: CTD_01
     * gia ban: 11000 VND
     * khoi luong: 11 KG
     */
    $ctd1 = new ChiTietDon();
    $ctd1->setMaSoChiTiet ('CTD_01');
    $ctd1->setGiaBan (11000);
    $ctd1->setKhoiLuong (11);


    /**
     * class: ChiTietDon
     * ma so chi tiet: CTD_02
     * gia ban: 12000 VND
     * khoi luong: 12 KG
     */
    $ctd2 = new ChiTietDon();
    $ctd2->setMaSoChiTiet ('CTD_02');
    $ctd2->setGiaBan (12000);
    $ctd2->setKhoiLuong (12);

    /**
     * class: ChiTietDon
     * ma so chi tiet: CTD_03
     * gia ban: 13000 VND
     * khoi luong: 13 KG
     */
    $ctd3 = new ChiTietDon();
    $ctd3->setMaSoChiTiet ('CTD_03');
    $ctd3->setGiaBan (13000);
    $ctd3->setKhoiLuong (13);

    /**
     * Class: Chi tiet phuc
     * ma so chi tiet: CTP_01
     * bao gom CTD_02, CTD_03
     */

    $ctp1 = new ChiTietPhuc();
    $ctp1->setMaSoChiTiet ('CTP_01');
    $arr23 = [$ctd2, $ctd3];
    $ctp1->setDanhSachChiTiet ($arr23);


    /**
     * class: ChiTietDon
     * ma so chi tiet: CTD_04
     * gia ban: 14000 VND
     * khoi luong: 14 KG
     */
    $ctd4 = new ChiTietDon();
    $ctd4->setMaSoChiTiet ('CTD_04');
    $ctd4->setGiaBan (14000);
    $ctd4->setKhoiLuong (14);

    /**
     * class: ChiTietDon
     * ma so chi tiet: CTD_05
     * gia ban: 15000 VND
     * khoi luong: 15 KG
     */
    $ctd5 = new ChiTietDon();
    $ctd5->setMaSoChiTiet ('CTD_05');
    $ctd5->setGiaBan (15000);
    $ctd5->setKhoiLuong (15);

    /**
     * Class: Chi tiet phuc
     * ma so chi tiet: CTP_02
     * bao gom CTD_04, CTD_05
     */
    $ctp2 = new ChiTietPhuc();
    $ctp2->setMaSoChiTiet ('CTP_02');
    $arr45 = [$ctd4, $ctd5];
    $ctp2->setDanhSachChiTiet ($arr45);

    /**
     * class: ChiTietDon
     * ma so chi tiet: CTD_06
     * gia ban: 16000 VND
     * khoi luong: 16 KG
     */
    $ctd6 = new ChiTietDon();
    $ctd6->setMaSoChiTiet ('CTD_06');
    $ctd6->setGiaBan (16000);
    $ctd6->setKhoiLuong (16);

    /**
     * class: ChiTietDon
     * ma so chi tiet: CTD_06
     * gia ban: 16000 VND
     * khoi luong: 16 KG
     */
    $ctd7 = new ChiTietDon();
    $ctd7->setMaSoChiTiet ('CTD_07');
    $ctd7->setGiaBan (17000);
    $ctd7->setKhoiLuong (17);

    /**
     * class: ChiTietDon
     * ma so chi tiet: CTD_08
     * gia ban: 18000 VND
     * khoi luong: 18 KG
     */
    $ctd8 = new ChiTietDon();
    $ctd8->setMaSoChiTiet ('CTD_08');
    $ctd8->setGiaBan (18000);
    $ctd8->setKhoiLuong (18);

    /**
     * Class: Chi tiet phuc
     * ma so chi tiet: CTP_03
     * bao gom CTD_06, CTD_07, CTD_08
     */
    $ctp3 = new ChiTietPhuc();
    $ctp3->setMaSoChiTiet ('CTP_03');
    $arr678 = [$ctd6,$ctd7, $ctd8];
    $ctp3->setDanhSachChiTiet ($arr678);

    /**
     * Class: May
     * ma so may: MC_01
     * bao gom: CTD_01, CTP_01
     */
    $mc1 = new May();
    $mc1->setMaSoMay ("MC_01");
    $arr123 =[$ctd1,$ctp1];
    $mc1->setDanhSachChiTiet ($arr123);

    /**
     * Class: May
     *  ma so may: MC_02
     * bao gom: CTD_02, CTP_03
     */
    $mc2 = new May();
    $mc2->setMaSoMay ("MC_02");
    $arr45678 = [$ctp2,$ctp3];
    $mc2->setDanhSachChiTiet ($arr45678);

    /**
     * Class: Kho
     * bao gom MC_01, MC_02
     */
    $kho = new Kho();
    $kho->setMaSoKho ('KHO01');
    $arrmay12 = [$mc1,$mc2];
    $kho->setDanhSachMay ($arrmay12);

    /**
     * @return string
     */
    function option() : string {
        echo "\e[36mChon option ma ban muon\e[0m\n";
        echo "\e[36m1. Nhap may moi\e[0m\n";
        echo "\e[36m2. Xuat nhung may hien tai trong kho\e[0m\n";
        echo "\e[36m3. Tinh tien may theo ten cho truoc\e[0m\n";
        echo "\e[36m4. Tinh khoi luong may theo ten cho truoc\e[0m\n";
        echo "\e[36m0. Thoat khoi chuong trinh\e[0m\n";
        $res = readline ('');
        return $res;
    }

    while (true){
       $res= option ();
       switch ($res){
           case "1":
               $kho->nhap ();
               break;
           case "2":
               $kho->xuat ();
               break;
           case "3":
               echo "Vui long nhap ma may VD: MC_01...\n";
               $maSoMay = readline ('');
               print_r ($kho->timKiemMay ($maSoMay));
               break;
           case "4":
               echo "Vui long nhap ma may VD: MC_01...\n";
               $maSoMay = readline ('');
               print_r ($kho->tinhKhoiLuongMay($maSoMay));
               break;
           case "0":
               exit("\e[31m =><==><= .........STOPPING........=><==><= ....\e[0m \n");
           default:
               echo "\e[31mSai dinh dang, Vui long nhap lai\e[0m\n";
               break;
       }
    }
