<?php
require_once 'May.php';


class Kho
{
    /**
     * @var varchar
     */
    public $maSoKho;

    /**
     * @var array
     */
    public $danhSachMay=[];

    /**
     * @param varchar $maSoKho
     */
    public function setMaSoKho($maSoKho)
    {
        $this -> maSoKho = $maSoKho;
    }

    /**
     * @param array $danhSachMay
     */
    public function setDanhSachMay($danhSachMay)
    {
        $this -> danhSachMay = $danhSachMay;
    }

    /**
     * @return void
     */
    public function nhap(){
        while (true){
            $option = readline ('Ban co mon tiep tuc nhap??? 1. Continue 0.Stop');
            switch ($option){
                case "1":
                    $may = new May();
                    $may->nhap ();
                    $this->danhSachMay[]=$may;
                    break;
                case "0":
                    echo "\e[31m STOP: dung lai qua trinh nhap chi tiet phuc ....\n";
                    echo "\033[0m";
                    return;
                default:
                    echo  "\e[31m Nhap sai dinh dang, vui long nhap lai \n";
                    echo "\033[0m";
                    break;
            }
        }
    }

    /**
     * @return void
     */
    public function xuat(){
        echo "\e[34m =.= Kho ".$this->maSoKho." ,bao gom: \e[0m \n";
        for ($i=0;$i<count ($this->danhSachMay);$i++){
            $this->danhSachMay[$i]->xuat();
        }
    }

    /**
     * @param $maSomMay
     * @return string
     */
    public function timKiemMay($maSomMay){
        for ($i=0;$i<count ($this->danhSachMay);$i++){
            if ($this->danhSachMay[$i]->getMaSoMay($maSomMay)){
                return $this->danhSachMay[$i]->xuat()."======================================\n\e[32mTONG TIEN CUA MAY: "
                    .$this->danhSachMay[$i]->tinhTongTien()." VND\e[0m \n";
            }
        }
        return "Khong tim thay san pham";
    }

    /**
     * @param $maSomMay
     * @return string
     */
    public function tinhKhoiLuongMay($maSomMay){
        for ($i=0;$i<count ($this->danhSachMay);$i++){
            if ($this->danhSachMay[$i]->getMaSoMay($maSomMay)){
                return $this->danhSachMay[$i]->xuat()."======================================\n\e[32mTONG KHOI LUONG CUA MAY: "
                    .$this->danhSachMay[$i]->tinhTongKhoiLuong()."KG\e[0m \n";
            }
        }
        return "Khong tim thay san pham";
    }
}

