<?php

require_once 'ChiTiet.php';
require_once 'ChiTietDon.php';

class ChiTietPhuc extends ChiTiet
{
    /**
     * @var array
     */
    public $danhSachChiTiet = [];

    /**
     * @return void
     */
    public function nhap()
    {
        parent ::nhap ();
        while (true) {
            $loaiChiTiet = readline ('1.Chi Tiet Don, 2.Chi Tiet Phuc, 0 thoat. Ban muon nhap chi tiet nao ??(chi 1 hoac 2 hoac 0)');
            switch ($loaiChiTiet) {
                case "1":
                    $item = new ChiTietDon();
                    $item -> nhap ();
                    $this -> danhSachChiTiet[] = $item;
                    break;
                case "2":
                    $item = new ChiTietPhuc();
                    $item -> nhap ();
                    $this -> danhSachChiTiet[] = $item;
                    break;
                case "0":
                    echo "\e[31m STOP: dung lai qua trinh nhap chi tiet phuc ....\n";
                    echo "\033[0m";
                    return;
                default:
                    echo "\e[31m Nhap sai dinh dang, vui long nhap lai \n";
                    echo "\033[0m";
                    break;
            }
        }
    }

    /**
     * @return void
     */
    public function xuat()
    {
        parent ::xuat ();
        echo "bao gom:\n";
        for ( $i = 0 ; $i < count ($this -> danhSachChiTiet) ; $i ++ ) {
            $this -> danhSachChiTiet[ $i ] -> xuat ();
        }
    }

    /**
     * @param array $danhSachChiTiet
     * @return void
     */
    public function setDanhSachChiTiet($danhSachChiTiet)
    {
        $this -> danhSachChiTiet = $danhSachChiTiet;
    }

    /**
     * @return float
     */
    public function tinhTongKhoiLuong(): float
    {
        $tongKhoiLuong=0;
        for ( $i = 0 ; $i < count ($this -> danhSachChiTiet) ; $i ++ ) {
            $tongKhoiLuong+=  $this -> danhSachChiTiet[ $i ] -> tinhTongKhoiLuong ();
        }
        return $tongKhoiLuong;
    }

    /**
     * @return int
     */
    public function tinhTongTien(): int
    {
        $tongTien=0;
        for ( $i = 0 ; $i < count ($this -> danhSachChiTiet) ; $i ++ ) {
            $tongTien+=  $this -> danhSachChiTiet[ $i ] -> tinhTongTien ();
        }
        return $tongTien;
    }
}